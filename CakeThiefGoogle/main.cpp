#include<iostream>
#include<vector>

using namespace std;

class Cake
{
    public:
	   int weight;
	   int value;
    
	   Cake(int wt , int val)
	   {
           weight = wt;
           value  = val;
       }
};

class Engine
{
    private:
	   vector< vector<int> > matrix;
	   vector<Cake> cakeWtNVal;
	   int maxWeight;
	   
	   void initializeMatrix(int totalWtNVal)
	   {
           for(int i = 0 ; i < totalWtNVal ; i++)
           {
               vector<int> innerMat;
               for(int i = 0 ; i <= maxWeight ; i++)
               {
                   innerMat.push_back(0);
               }
               matrix.push_back(innerMat);
           }
       }
    
    public:
	   Engine(vector<int> weights , vector<int> values , int mWt)
	   {
           maxWeight = mWt;
           int size  = (int)weights.size();
           for(int i = 0 ; i < size ; i++)
           {
               Cake cake = Cake(weights[i] , values[i]);
               cakeWtNVal.push_back(cake);
           }
           initializeMatrix(size);
       }
    
	   int findMaxValue()
	   {
           int outerSize = (int)matrix.size();
           for(int i = 0 ; i < outerSize ; i++)
           {
               int innerSize = (int)((matrix[i]).size());
               for(int j = 0 ; j <= innerSize ; j++)
               {
                   int comp1 = i > 0 ? matrix[i-1][j] : matrix[i][j];
                   int comp2 = j > cakeWtNVal[i].weight ? cakeWtNVal[i].value + matrix[i][j-cakeWtNVal[i].weight] : 0;
                   matrix[i][j] = max(comp1 , comp2);
               }
           }
           return matrix[outerSize-1][(int)(matrix[0].size())];
       }
	   
	   void printCakeVector()
	   {
           int size = (int)cakeWtNVal.size();
           for(int i = 0 ; i < size ; i++)
           {
               cout<<cakeWtNVal[i].weight<<" "<<cakeWtNVal[i].value<<endl;
           }
       }
};

int main()
{
    int weightsArr[] = {2,3,7};
    int valuesArr[]  = {15,90,160};
    vector<int> weights;
    vector<int> values;
    int size = sizeof(weightsArr)/sizeof(weightsArr[0]);
    for(int i = 0 ; i < size ; i++)
    {
        weights.push_back(weightsArr[i]);
        values.push_back(valuesArr[i]);
    }
    Engine e = Engine(weights , values , 20);
    //e.printCakeVector();
    cout<<e.findMaxValue()<<endl;
    return 0;
}
